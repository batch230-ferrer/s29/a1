db.users.find(
    {
        $or: [
            {firstName: "Stephen"},
            {lastName: "Doe"}
        ]	
    },
    {
        _id: 0,
        age: 0,
        contact:0,
        courses:0,
        department:0
    }
);


db.users.find(
    {
        $and: [
            {age: {$gte:70}},
            {department: "HR"}
        ]
    }
)



db.users.find(
    {
        $and: [
            {age: {$lte:30}},
            {firstName: {
                $regex: 'e', $options: '$I'}
            }
        ]
    }
)
